classdef SlidebookObj < handle
    %2PDATA Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Access=public)
        
        File
        Folder
        Link
        DateStr
        TimeStr
        DaqFile
        ParameterFile
    end
    
    properties (Transient)
        Daq
        Frames 
        unattended = 0;
    end
    
    properties (Hidden)
        Log
        TifReg
        Tiff
        AIrate = 10000;
        ifi
        frametimes        
        trialparams = [];        
        ExpParams
        polexp = 0;
        AverageFrame
        imF0
        
        TrialStartFrames        
        TrialStartDAQsamples
        
        TrialEndFrames
        TrialEndDAQsamples
        
        TrialAngles
        AngleStep
        
        ROI = struct('name',[],'mask',[],'coords',[])

    end
    
    methods (Access=public)
        
        function obj = SlidebookObj(pathIN)
            % Constructor
            
            if nargin == 0
                obj = getTiffPathFromGUI(obj);
            end
            
            if nargin >0
                if isa(pathIN,'char')
                    obj = getTiffPathFromARG(obj,pathIN);
                else
                    error('Please input path to tiff file as a string, or leave empty')
                end
            end
            
            obj.Tiff = [obj.File '.tiff'];
            obj.TifReg = [obj.File '._reg.tif'];
            obj.Link = ['<a href="matlab:winopen(''' obj.Folder ''')">open folder</a>'];            
            getLogData(obj);
            getDaqFile(obj);
            getParamFile(obj);          
            
        end
        
    end
    
    methods (Access=public)
        
        runTifReg(obj, poolSiz, walkThresh); % Run ca_RegSeries_v3 on tiff file
        
        setPolExp(obj); % Some parameters specific for polarizer experiments
        plotPolAvgs(obj,angle1,angle2); % Some scrappy script for pol plots - tidy up
        
        adhocF0(obj); % Get an average image for use as F0 
         
        varargout = getFrames(obj,scan_extent); % Read tiff file and store image frames temporarily
        imavg(obj); % Display AverageFrame for the tiff file
        [frameidx] = nextframe(obj,sampleidx);% Shortcut to find the next/prev frame closest 
        [frameidx] = prevframe(obj,sampleidx); % to a particular sample index in DAQ AI channels
        [Ftrace] = scanROI(obj, mask, scan_extents);
        getAuxDaqData(obj); % Gets values of DAQ AI within trials
        getTrialtimes(obj); % Gets trialstarts / stops in terms of frame number
        % Will auto-run getFrametimes if missing
        varargout = getFrametimes(obj,scan_extent);  % Will auto-run getDaqData if missing
        getDaqData(obj); % Read daq file and store channel data temporarily
        getParameters(obj); %Read parameter file and store in ExpParams

        fig = play(obj,vid_extent)
        fig = nia_sbo_playFlatMovie(mov, acceptROIs)
        %         play
        %get channel data
        %create subclass objects for trials
        %
        %draw rois
        % get F values in ROIs
        % save in trial objects
        % plot F values
        % store trial frames
        
    end
    
    
end

