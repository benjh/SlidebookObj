function adhocF0(obj)
% Define a region on the DAQ AI channels and average frames recorded during
% that sequence

figure(91)
plot(obj.Daq(:,2:3))
title([{'Drag the box around an area with no stimulus activity.'}, ...
    {'Frames within this region will be averaged for use as an F0 image.'}])
h = imrect;
% pos = wait(h)
pause
pos = getPosition(h);
setmin = round(pos(1));
setmax = round(pos(3) + setmin);
if setmin<0, setmin = 1; end
if setmax>length(obj.Daq), setmax = length(obj.Daq); end
delete(h); clear h
close(figure(91))
% ylims = ylim;
% hold on,
% line([setmin setmin], [ylims(1) ylims(2)],'color','r','linewidth',2,'linestyle',':');
% line([setmax setmax], [ylims(1) ylims(2)],'color','r','linewidth',2,'linestyle',':');

% Accumulate frames 
F0frames = obj.Frames(:,:, [ prevframe(obj,setmin) : prevframe(obj,setmax)] );
% Average
imF0 = mean(F0frames,3);
% Push to obj
obj.imF0 = imF0;