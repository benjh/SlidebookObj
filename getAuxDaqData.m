function getAuxDaqData(obj)

if isempty(obj.TrialStartFrames)
    getTrialtimes(obj);
end

if obj.polexp
    
    obj.AngleStep = 720 / (length(obj.ExpParams.w_trials)+2);
    % + 2 corresponds to 0 degree trials
    
    % Angle can be found as follows: 360*daq(:,2)/5
    for idx = 1:length(obj.TrialStartDAQsamples)
    obj.TrialAngles(idx) =  obj.AngleStep*round(360*0.2*mean( obj.Daq( ...
        obj.TrialStartDAQsamples(idx) : obj.TrialEndDAQsamples(idx) ...
        , 2)) / obj.AngleStep );
    end    
    
end