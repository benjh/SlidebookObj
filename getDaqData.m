function getDaqData(obj)  
% Read DAQ file. Get frametimes etc through additional functions
unattended = obj.unattended;
% if nargin < 2 
%     unattended = 0;
% else
%     assert( (unattended == 1 | unattended == 0 ), 'Input argument getFrames(obj,unattended) must be 0 or 1.');
% end

% Check if Frames already exists in the object
if isempty(obj.DaqFile) && ~( unattended )
    obj.DaqFile = uigetfile('*.daq','Please specify the DAQ file',obj.Folder);
elseif isempty(obj.DaqFile) && unattended 
    disp(['DAQ file not found. ' [obj.File] ' skipped'])
    return
end

disp(['Reading DAQ file'])

obj.Daq = daqread([obj.Folder obj.DaqFile]);

