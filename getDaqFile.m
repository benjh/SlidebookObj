function getDaqFile(obj)
% Find the path to the DAQ file saved by Matlab during an experiment
% Shortcuts for filenames:
try
daqTimeTry = str2num(obj.TimeStr);
daqFile = getDaqPath(obj, daqTimeTry);
daqPath = [obj.Folder daqFile];

%Check the file exists before storing it
if exist(daqPath, 'file') ~= 2
    % Daq file may be created up to 1 minute before 2p capture started
    daqTimeTry = daqTimeTry - 1;
    daqFile = getDaqPath(obj, daqTimeTry);
    daqPath = [obj.Folder daqFile];

    %Check the new file exists before storing it
    if exist(daqPath, 'file') == 2
        obj.DaqFile = daqFile;
    else
        disp(['Error: could not find ' daqFile ' in folder <a href="matlab:winopen(''' obj.Folder ''')">(open)</a> '])
    end
else
    obj.DaqFile = daqFile;
end
catch
end

function daqfiletry = getDaqPath(obj,exptime)

daqfiletry = strcat(['aq_EXP_MASTER_20' obj.DateStr '_' num2str(exptime,'%.4d') '.daq']);

