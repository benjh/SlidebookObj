function varargout = getFrames(obj,scan_extent)
% Reads all 'directories' (frames) within the SlidebookTiff object 
% Stores in a transient variable which will not be saved with the object.
% Setting 'unattended' option to 1 will skip all requests for user input
% and default to using existing files. 
unattended = obj.unattended;
if nargin < 2 
    scan_extent = [1 0];
end

% Check if Frames already exists in the object
if ~( isempty(obj.Frames) ) && ~( unattended )
    
    % Query user for action to take
    overwriteflag = 9;
    while ( ~(overwriteflag == 0) && ~(overwriteflag == 1) ) || ~isa(overwriteflag,'double') 
        overwriteflag =  input(['Frames already exist. ' ...
            '\nOverwrite   [1] ' ...
            '\nQuit        [0]' ...
            '\nEnter selection: ']);
    end
    if ~overwriteflag
        return
    end

end 

% Now, check if we've got a registered Tiff 

FileName = [obj.Folder obj.TifReg];

if ~exist(FileName,'file')
    
    if ~unattended
        
        whichtiffflag = 99;
        while ~(ismember(whichtiffflag, [1,4,8,0]) ) || ~isa(whichtiffflag,'double')
            whichtiffflag =  input(['No registered tiff file found (' [obj.Link] '). ' ...
                '\nRun registration now (default settings) [1] ' ...
                '\nBrowse for registered tiff elsewhere    [4] ' ...
                '\nContinue with un-registered .tiff       [8]' ...
                '\nQuit                                    [0]' ...
                '\nEnter selection: ']);
        end
        if ~whichtiffflag
            return
        end
    
    else % Use existing tiff file when in unattended mode
        whichtiffflag = 8;
    end
    
    % Otherwise, carry out the user's choice:
    switch whichtiffflag
        
        case 1 % Run registration now (default settings)
            runTifReg(obj);     
           
        case 4 % Browse for registered tiff elsewhere  
            [File,Path] = uigetfile('*.tif*');
            FileName = [Path File];           
            
        case 8 % Continue with un-registered .tiff
            FileName = [obj.Folder obj.Tiff];
            disp('Using existing unregistered .tiff')
    end
        
end

% Now we have the correct reg.tif or tiff, read in frames:

s = warning('off','all');

tifobj = Tiff([FileName],'r');  % Open Tiff file as an object. Read-only mode.

% First get the number of frames:
setDirectory(tifobj,1)          % Make sure we're at the first frame.
n=0;
while ~lastDirectory(tifobj)
    n=n+1;
    nextDirectory(tifobj);
end

% Initialize stkStruct:
stkStruct = zeros( getTag(tifobj,'ImageLength'), getTag(tifobj,'ImageWidth'), currentDirectory(tifobj) , 1);

% Now read tiff image data from each directory:
setDirectory(tifobj,scan_extent(1))  % Make sure we're at the first frame (default),
% or the first within the user specified scan_extent
n=0;
while ~lastDirectory(tifobj)
    % Last frame is never read, but this shouldn't be an issue... hopefully!
   
    n=n+1;
    
    stkStruct(:,:,n) = read(tifobj);
    
    % If scan_extent was specified we will just break the loop at the last
    % frame requested
    if currentDirectory(tifobj) == scan_extent(2) 
        break
    end
    
    nextDirectory(tifobj);

end
disp([num2str(n) ' frames returned.'])

close(tifobj); clear tifobj

% Restore previous warning state
warning(s);

if nargin<2
    obj.Frames = stkStruct;
    obj.AverageFrame = mean(stkStruct,3);
else
    varargout{1} = stkStruct;
end
% set(obj,'Frames', stkStruct);

