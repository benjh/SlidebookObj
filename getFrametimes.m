function getFrametimes(obj)
% Read DAQ AI0 / wstim(:,1) 
% Detect frametimes (as DAQ sample indices) and store in object

if isempty(obj.Daq)
    getDaqData(obj);
end
wstim = obj.Daq;

airate = obj.AIrate;


% For each tiff frame, DAQ ai0 records a pulse:
wframes=find(diff(round(wstim(:,1)/5))==1);%
% wframes(1) first grabbed frame
% wframes(end) last grabbed frame

% Get rid of any duplicate frames resulting from double blips in signal
wframes(find(diff(wframes)<0.1*mean(diff(wframes))) + 1) = [];

% Get inter-frame interval (in samples)
ifi = ceil(mean(diff(wframes)));

% Push to object
obj.ifi = ifi;
obj.frametimes = wframes;
    
    %%%%%% 
% % % %     
% % % %     % Find trial start/stop times (in samples)
% % % %     trialstarts = find(diff(wstim(:,2))>=0.3 & diff(round(wstim(:,2))) <10 );
% % % %     trialends = find(diff(round(wstim(:,2))*10)<=-1 & diff(round(wstim(:,2)*10)) > -100 );
% % % %     trialstarts(find(diff(trialstarts)<100)+1) = [];
% % % %     trialends(find(diff(trialends)<100)+1) = [];
% % % %     
% % % %     % First trialstart may be missed if wstim(:,2) was not reset to 0 at start
% % % %     if round(mean(wstim(1:100,2)*10)) ~= 0 
% % % %         % A trialend may also be erroneously detected at the start, so swap
% % % %         % these around:
% % % %         if (trialends(1) < trialstarts(1))
% % % %             trialstarts(2:length(trialstarts)+1) = trialstarts;
% % % %             trialstarts(1) = trialends(1);
% % % %             trialends(1) = [];
% % % %         end
% % % %         
% % % %     end
% % % %     % Plot to check trial start/stops
% % % % %     if PLOT_START_STOP_CHECKS
% % % %         figure
% % % %         plot(wstim(:,2))
% % % %         hold on
% % % %         plot(trialstarts,wstim(trialstarts,2),'k*')
% % % %         plot(trialends,wstim(trialends,2),'g*')
% % % %         hold off
% % % %         legend('wstim(:,2)',['Trial starts (' num2str(length(trialstarts)) ')'],['Trial stops (' num2str(length(trialends)) ')'])
%         pause
%     end
    
%     % If we still don't have the correct num of trials: stop and check
%     if length(trialstarts)~=stream(didx).info.Ntrials
%         
%         if ( stream(didx).info.exp_date == 170714 ...
%                 && stream(didx).info.exp_time == 928 ) ...
%             || ( stream(didx).info.exp_date == 170714 ...
%                 && stream(didx).info.exp_time == 1222 ) ...
%                 ||  ( stream(didx).info.exp_date == 170713 ...
%                 && stream(didx).info.exp_time == 1309 ) 
%             
%             % Special case for a file in which the start of the first trial 
%             % wasn't registered
%             trytriallength = mean(trialends(2:end)-trialstarts);
%             trialstarts(2:length(trialstarts)+1) = trialstarts;
%             trialstarts(1) = trialends(1) - trytriallength;
%         else
%             error(['Some trials not detected: didx = ' num2str(didx)])
%         return
%         end
%     end
    
%     % Get the trial length in seconds and check this is in the right
%     % ballpark:
%     triallength_s = (trialends - trialstarts)/10000;
%     if mean(triallength_s) > (time_rest + 2*time_expt)*1.1  ||  mean(triallength_s) < (time_rest + 2*time_expt)*0.9
%         error(['Error in trial length: didx = ',num2str(didx),', tidx = ',num2str(tidx)])
%     end
    

% % % % % %     % For each trial find some parameters from the DAQ file:
% % % % % %     clear pat_num seq_num
% % % % % %     for tidx = 1:length(trialstarts)
% % % % % %         
% % % % % %         % First find the closest frames to the trial start/stop times:
% % % % % %         trystartframe = nextframe(trialstarts(tidx),ifi,wframes);
% % % % % %         if length(trystartframe)>1
% % % % % %             % 2 Frames caught in window - delete the later one
% % % % % %             trystartframe(2) = [];
% % % % % %         elseif isempty(trystartframe)
% % % % % %             % No frames found - extend the window by 2 timepoints
% % % % % %             trystartframe = nextframe(trialstarts(tidx)+2,ifi,wframes);
% % % % % %         end
% % % % % %         trialstartframe(tidx,1) = trystartframe;
% % % % % %         
% % % % % %         trystopframe = prevframe(trialends(tidx),ifi,wframes);
% % % % % %         if length(trystopframe)>1
% % % % % %             % 2 Frames caught in window - delete the earlier one
% % % % % %             trystopframe(1) = [];
% % % % % %         elseif isempty(trystartframe)
% % % % % %             % No frames found - extend the window by 2 timepoints
% % % % % %             trystopframe = prevframe(trialstops(tidx)-2,ifi,wframes)
% % % % % %         end
% % % % % %         trialstopframe(tidx,1) = trystopframe;
% % % % % %         
% % % % % %         % Get pattern number: voltage amplitude wstim(:,2) x 5
% % % % % %         pat_num(tidx) = round(5*mean(wstim(trialstarts(tidx):trialends(tidx),2)));
% % % % % %         
% % % % % %         % Get sequence number: voltage amplitude wstim(:,3) x 5
% % % % % %         % Use this to check these extracted paramters later
% % % % % %         seq_num(tidx) = round(5*mean(wstim(trialstarts(tidx):trialends(tidx),3)));
% % % % % %         
% % % % % %     %{
% % % % % %         %%%%%% OPTIC FLOW SEQUENCE %%%%%
% % % % % %         
% % % % % %         % Guess optic flow start/stop times
% % % % % %         tryOFstart(tidx) = trialstarts(tidx) + airate*(0.5*time_rest );
% % % % % %         tryOFstop(tidx) = trialstarts(tidx) + airate*(0.5*time_rest + time_expt);
% % % % % %         timetol = airate*time_expt*0.25;
% % % % % %         OFvel(tidx) = round(mean(gradient(wstim(tryOFstart(tidx)+timetol:tryOFstop(tidx)-timetol,4),1/airate)));
% % % % % %         % airate/50 = panels refresh rate
% % % % % %         
% % % % % %         % Try to find precise start time (if OF vel is not equal to 0)
% % % % % %         clear OFpks OFstartpks
% % % % % %         OFgrad = gradient(unwrap(0.2*pi*wstim(tryOFstart(tidx)+timetol:tryOFstop(tidx)-timetol,4))/(0.2*pi),1/airate);
% % % % % %         [~,OFpks] = findpeaks(abs(OFgrad));
% % % % % %         OFpks = OFpks(abs(OFgrad(OFpks))>6*std(OFgrad));
% % % % % %         iPki = round(mean(diff(OFpks)));
% % % % % %         OFvel_estimate = round(mean(OFgrad(OFpks))); %Could convert this to actual gain, based on SpFreq
% % % % % %         
% % % % % %         if abs(sign(OFvel(tidx))) % OF does move..
% % % % % %             % Beginning section of OF stim signal which includes the ON point
% % % % % %             OFstartsec = unwrap(0.2*pi*wstim(tryOFstart(tidx)-0.25*timetol:tryOFstart(tidx)+0.25*timetol,4))/(0.2*pi);
% % % % % %             OFstartsec = OFstartsec - mean(OFstartsec(1:100));
% % % % % %             
% % % % % %             % Find peaks in stim velocity signal
% % % % % %             OFstartgrad = gradient(OFstartsec,1/airate);
% % % % % %             [~,OFstartpks] = findpeaks(abs(OFstartgrad));
% % % % % %             
% % % % % %             % Get rid of small peaks which were detected
% % % % % %             OFstartpks = OFstartpks(abs(OFstartgrad(OFstartpks))>6*std(OFgrad));
% % % % % %             
% % % % % %             OFstartpks = OFstartpks(abs(OFstartgrad(OFstartpks)) > abs(0.6*OFvel_estimate));
% % % % % %             
% % % % % %             % Only keep the peaks which have an appropriate time interval
% % % % % %             % (based on average of middle OF section)
% % % % % %             while ~ ( OFstartpks(2) < OFstartpks(1) + iPki*1.1 && OFstartpks(2) > OFstartpks(1) + iPki*0.9 ),
% % % % % %                 OFstartpks(1) = [];
% % % % % %             end
% % % % % %             OFstart(tidx) = OFstartpks(1) + tryOFstart(tidx)-0.25*timetol;
% % % % % %             startdiff(tidx) = OFstart(tidx) - tryOFstart(tidx);
% % % % % %             
% % % % % %         end
% % % % % %         
% % % % % %       %}
% % % % % %         
% % % % % %     end
% % % % % %     
% % % % % %     
    
    
    
    
    
    
  %{
    for tidx = 1:length(trialstarts)
        if ~abs(sign(OFvel(tidx))) % OF does not move..
            % Just guess start time based on average error in the other trials
            OFstart(tidx) = tryOFstart(tidx) + median(startdiff);
        end
        % Get next frame after OF starts (for all trials)
        OFstartframe(tidx) = nextframe(OFstart(tidx),ifi,wframes);
    end
  %}
    
    
    %%%%%% GRATING SEQUENCE %%%%%%
    %{
    for tidx = 1:length(trialstarts)
        % Guess grating start/stop times
        tryGstart(tidx) = trialstarts(tidx) + airate*(0.5*time_rest + time_expt);
        tryGstop(tidx) = trialstarts(tidx) + airate*(0.5*time_rest + 2*time_expt);
        
        clear Gpks Gstartpks Gstoppks
        
        % So long as the stimulus includes a grating..
        if pat_num(tidx) ~= 26
            
            % Estimate the grating velocity (won't detect velocity = 0)
            Ggrad = gradient(unwrap(0.2*pi*wstim(tryGstart(tidx)+timetol:tryGstop(tidx)-timetol,5))/(0.2*pi),1/airate);
            [~,Gpks] = findpeaks(abs(Ggrad));
            Gpks = Gpks(abs(Ggrad(Gpks))>2*std(Ggrad));
            Gvel_estimate = round(mean(Ggrad(Gpks))); %Could convert this to actual gain, based on SpFreq
            Gvel(tidx) = ygain(sign(ygain)==sign(Gvel_estimate));
            iPki = round(mean(diff(Gpks)));
            
            % Try to find precise start time (if OF vel is not equal to 0)
            
            % Beginning section of Grating stim signal which includes the ON point
            Gstartsec = unwrap(0.2*pi*wstim(tryGstart(tidx)-0.25*timetol:tryGstart(tidx)+0.25*timetol,5))/(0.2*pi);
            Gstartsec = Gstartsec - mean(Gstartsec(1:100));
            
            % Find peaks in stim velocity signal
            Gstartgrad = gradient(Gstartsec,1/airate);
            [~,Gstartpks] = findpeaks(abs(Gstartgrad));
            
            % Get rid of small peaks which were detected
            Gstartpks = Gstartpks(abs(Gstartgrad(Gstartpks)) > abs(0.5*Gvel_estimate));
            
            % Only keep the peaks which have an appropriate time interval
            % (based on average of middle G section)
            while ~ ( Gstartpks(2) < Gstartpks(1) + iPki*1.1 && Gstartpks(2) > Gstartpks(1) + iPki*0.9 )
                Gstartpks(1) = [];
            end
            
            % Update Grating start point estimate
            Gstart(tidx) = Gstartpks(1) + tryGstart(tidx)-0.25*timetol;
            Gstartdiff(tidx) = Gstart(tidx) - tryGstart(tidx);
            
            
            % Find grating stop point
            % End section of Grating stim signal which includes the OFF point
            Gstopsec = unwrap(0.2*pi*wstim(tryGstop(tidx)-0.25*timetol:tryGstop(tidx)+0.25*timetol,5))/(0.2*pi);
            Gstopsec = Gstopsec - mean(Gstopsec(1:100));
            
            % Find peaks in stim velocity signal
            Gstopgrad = gradient(Gstopsec,1/airate);
            [~,Gstoppks] = findpeaks(abs(Gstopgrad));
            
            % Get rid of small peaks which were detected
            Gstoppks = Gstoppks(abs(Gstopgrad(Gstoppks)) > abs(0.5*Gvel_estimate));
            
            % Only keep the peaks which have an appropriate time interval
            % (based on average of middle G section)
            while ~ ( Gstoppks(end) < Gstoppks(end-1) + iPki*1.1 && Gstoppks(end) > Gstoppks(end-1) + iPki*0.9 ),
                Gstoppks(end) = [];
            end
            
            % Update Grating stop point estimate
            Gstop(tidx) = Gstoppks(end) + tryGstop(tidx)-0.25*timetol;
            Gstopdiff(tidx) = Gstop(tidx) - tryGstop(tidx);
            
        end
    end
    %}
    
    %{
    for tidx = 1:length(trialstarts)
        if pat_num(tidx) == 26
            Gvel(tidx) = 0;
            % Just guess start stop times based on average error in the other trials
            Gstart(tidx) = tryGstart(tidx) + median(Gstartdiff);
            Gstop(tidx) = tryGstop(tidx) + median(Gstopdiff);
            
        end
        % Get next frame after Grating starts/last frame before it stops
        Gstartframe(tidx) = nextframe(Gstart(tidx),ifi,wframes);
        Gstopframe(tidx) = prevframe(Gstop(tidx),ifi,wframes);
        
        % Plot to check detection of different DAQ segments - pauses on
        % each trial
        if PLOT_FRAMETIME_CHECKS
            t = [1:length(wstim)];
            figure(2), hold off
            plot(t(trialstarts(tidx)-timetol : trialends(tidx)+timetol), wstim(trialstarts(tidx)-timetol : trialends(tidx)+timetol,4:5),'k')
            hold on
            plot(t(trialstarts(tidx):OFstart(tidx)),wstim(trialstarts(tidx):OFstart(tidx),1),'b')
            plot(t(OFstart(tidx):Gstart(tidx)),wstim(OFstart(tidx):Gstart(tidx),1),'g')
            plot(t(Gstart(tidx):Gstop(tidx)),wstim(Gstart(tidx):Gstop(tidx),1),'r')
            plot(t(Gstop(tidx):trialends(tidx)),wstim(Gstop(tidx):trialends(tidx),1),'b')
            legend('wstim','','pre','OF','G','post')
            xlim([trialstarts(tidx)-timetol trialends(tidx)+timetol])
            pause
            hold off
            
        end
    end
    %}
    
    %{
    %%%%%%% STORE EXTRACTED STIM PARAMETERS IN stream %%%%%%
    for tidx = 1:length(trialstarts)
        % Just to check whether the data extracted from DAQ file match the parameter file
        if ~old_fEXP
            trialparams = stream(didx).info.w_trials(seq_num(tidx),:);
            if pat_num(tidx) ~= 26
                if pat_num(tidx) ~= trialparams(1) && ...
                        Gvel(tidx) ~= trialparams(4) && ...
                        OFvel(tidx) ~= trialparams(5)
                    error(['DAQ/Param file mismatch, didx = ' num2str(didx),' tidx = ' num2str(tidx)])
                    return
                end
            else % Don't check grating velocity for OF-only pattern
                if pat_num(tidx) ~= trialparams(1) && ...
                        OFvel(tidx) ~= trialparams(5)
                    error(['DAQ/Param file mismatch, didx = ' num2str(didx),' tidx = ' num2str(tidx)])
                    return
                end
                
            end
        else % ao markers were different:
            % w_trials already randomized
            % wstim(:,2)   i*5/size(w_trials,1)
            % wstim(:,3)   useless
            % Just check whether velocities were picked up correctly
            trialparams = stream(didx).info.w_trials((tidx),:);
            if pat_num(tidx) > 0
                if Gvel(tidx) ~= trialparams(4) && ...
                        OFvel(tidx) ~= trialparams(5)
                    error(['DAQ/Param file mismatch, didx = ' num2str(didx),' tidx = ' num2str(tidx)])
                    return
                else
                    pat_num(tidx) = trialparams(1);
                end
            else % Don't check grating velocity for OF-only pattern
                if OFvel(tidx) ~= trialparams(5)
                    error(['DAQ/Param file mismatch, didx = ' num2str(didx),' tidx = ' num2str(tidx)])
                    return
                else
                    pat_num(tidx) = 26;
                    GVel(tidx) = 0;
                end
                
            end
        end
        
        % Work out what the unique stim sequence number was, for example
        % [1:~15], with [1:3] trials (repetitions), from the seq_num [1:~45]
        % This calculation was different for old experiments:
        clear sorted_trials_num
        if length(trialstarts) == 28
            sortedseq_num = ceil(0.5*seq_num(tidx));
        elseif length(trialstarts) == 14
            sortedseq_num = seq_num(tidx);
        elseif stream(didx).info.exp_date >= 170714
            sortedseq_num = ceil(seq_num(tidx)/3);
            elseif stream(didx).info.exp_date == 170713
            sortedseq_num = seq_num(tidx);
        end
        
        % Check if there have already been trials of this stim sequence
        if stream(didx).info.exp_date < 170714
            T = length(find(seq_num(1:tidx-1)==seq_num(tidx))) + 1;
        elseif stream(didx).info.exp_date == 170713
            T = length(find(ceil(seq_num(1:tidx-1)/2)==sortedseq_num)) + 1;
        else
            T = length(find(ceil(seq_num(1:tidx-1)/3)==sortedseq_num)) + 1;
        end

        stream(didx).stim(sortedseq_num,T).Tstart = trialstartframe(tidx);
        stream(didx).stim(sortedseq_num,T).Tstop = trialstopframe(tidx);
        
        stream(didx).stim(sortedseq_num,T).Gstart = Gstartframe(tidx);
        stream(didx).stim(sortedseq_num,T).Gstop = Gstopframe(tidx);
        stream(didx).stim(sortedseq_num,T).Gvel = Gvel(tidx);
        
        stream(didx).stim(sortedseq_num,T).OFstart = OFstartframe(tidx);
        stream(didx).stim(sortedseq_num,T).OFvel = OFvel(tidx);
        
        stream(didx).stim(sortedseq_num,T).pat_num = pat_num(tidx);
        
        % Check pat_name from the sequence matches the pattern file on
        % the Y:\ drive
        stream(didx).stim(sortedseq_num,T).pat_name =  pat_files_on_disk.mat{pat_num(tidx)};
        
        % And check the spatial frequency also matches the one in the pattern filename
        spFreqstr = regexpi(stream(didx).stim(sortedseq_num,T).pat_name, ('\d*.\d*(?=SpFreq)'), 'match' );
        if ~isempty(spFreqstr)
            stream(didx).stim(sortedseq_num,T).SpFreq = str2num(spFreqstr{:});
        else
            stream(didx).stim(sortedseq_num,T).SpFreq = 0;
        end
        
        % Since we checked earlier, we know the location of the grating will be
        % the same across all sequences, so just copy that info in here
        stream(didx).stim(sortedseq_num,T).Gpos = stream(didx).info.Gposition;
        
        % Store section of the DAQ signal for each sequence, in case we
        % want to check later: 
         % stream(didx).stim(sortedseq_num,T).DAQseries = wstim(trialstarts(tidx) : trialends(tidx), :);
         %  - seems unnecessary and adds >200MB
         
    end
    %}
    
    
  %{
    
    % Also, check whether seq_num extracted from DAQ file matches
    % rand_seq saved from experiment script. If not, this is one of
    % the first sets of experiments, where trials were incorrectly
    % randomized:
    stream(didx).info.RandomSeqError = ~isequal( stream(didx).info.w_randseq, seq_num );
    stream(didx).info.w_trials_sequence = seq_num;
    
    % Create a unique file name for the video data, into which all metadata
    % saved in stream(didx) is saved, PLUS associated tiff frames (below)
    posfieldnames{1,:} = 'frontal'; posfieldnames{2,:} = 'lateral';
    if stream(didx).info.Gposition == 0
        fnidx = 1;
    elseif stream(didx).info.Gposition == 72
        fnidx = 2;
    end
    % Update frames path in streamdata
    stream(didx).files.frames = strcat(['fly' num2str(stream(didx).info.fly) '_' posfieldnames{fnidx,:} '_' ...
        num2str(stream(didx).info.exp_date) '_' num2str(stream(didx).info.exp_time, '%04d') '.mat' ]);
  %}

% Shortcuts to find the next (or prev) frame from a particular timepoint in DAQ data
% 
% function frame = nextframe(sampidx)
% frame = find(obj.frametimes>=sampidx & obj.frametimes<sampidx+obj.ifi); % tolerance of 10
% 
% function frame = prevframe(sampidx,ifi,wframes) 
% frame = find(obj.frametimes<=sampidx & obj.frametimes>sampidx-obj.ifi);

