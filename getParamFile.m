function getParamFile(obj)
% Find the path to the PARAMETERS file saved by Matlab in Ben's experiments
% Shortcuts for filenames:
try
paramTimeTry = str2num(obj.TimeStr);
paramFile = getParamPath(obj, paramTimeTry);
paramPath = [obj.Folder paramFile];

%Check the file exists before storing it
if exist(paramPath, 'file') ~= 2
    % Parameter file may be created up to 1 minute before 2p capture started
    paramTimeTry = paramTimeTry - 1;
    paramFile = getParamPath(obj, paramTimeTry);
    
    %Check the new file exists before storing it
    if exist(paramPath, 'file') ~= 2
        disp(['Error: could not find: ' paramFile ' in <a href="matlab:winopen(''' obj.Folder ''')">tiff folder</a> '])
    else
        obj.ParameterFile = paramFile;
    end
    
else
   obj.ParameterFile = paramFile;
end
catch
end

function paramfiletry = getParamPath(obj,exptime)

paramfiletry = strcat(['aq_EXP_PARAMETERS_20' obj.DateStr '_' num2str(exptime,'%.4d') '.mat']);