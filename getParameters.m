function obj = getParameters(obj)
% First get the file name (assumes a single file per exp, correct for spatial_sum)
if isempty(obj.ParameterFile)    
    obj = getParamFile(obj);
end

params = load([obj.Folder obj.ParameterFile]);

obj.ExpParams = params;


% obj.Exp.time_rest = params.time_rest;
% obj.Exp.time_expt = params.time_expt;
% obj.Exp.xgain = params.xgain;
% % obj.Exp.ygain = params.ygain;
% obj.Exp.TempFreq = params.TempFreq;
% obj.Exp.w_trials = params.w_trials;
% obj.Exp.w_randseq = params.w_randseq';
% obj.Exp.pattern_order = params.pattern_order;

% obj.Exp.Ntrials = length(params.w_trials);





