function obj = getTiffPathFromARG(obj,pathIN)
% Extracts file name and folder when tiff path is specified in obj(InputArg)
if isa(pathIN,'char')
    
    folderExpr = '(.*?)\\';
    foldChar = regexpi(pathIN,folderExpr,'end');
    obj.Folder = pathIN(1:foldChar(end));
    
    pathINend = pathIN(foldChar(end)+1:end);
    if strcmp(pathINend(end-8:end),'._reg.tif')
        obj.File = pathINend(1:end-9);
    elseif strcmp(pathINend(end-3:end),'.tif')
        obj.File = pathINend(1:end-4);
    elseif strcmp(pathIN(end-4:end),'.tiff')
        obj.File = pathINend(1:end-5);
    else
        obj.File = pathINend;
    end
    
else
    
    error('Please input path to tiff file as a string, or leave empty')
    
end