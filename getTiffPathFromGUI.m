function obj = getTiffPathFromGUI(obj)
% Extracts file name and folder properties when tiff is selected through UIGETFILE
[FileName,PathName] = uigetfile('*.tif*');
tifPath = [PathName,FileName];
tifExpr = '.*?(?=._reg.tif|.tiff)';
PathExtCut = char(regexpi(tifPath,tifExpr,'match'));
fileExpr = '\\';
fileChar = char(regexpi(PathExtCut,fileExpr,'split'));
obj.File = fileChar(end,:);
obj.Folder = PathName;