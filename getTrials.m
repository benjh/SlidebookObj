function [trialstart trialstop] = getTrials( )
% INPUTS:
%        wstim3   -   DAQ ai channel time-series with trial start/stop indicators 
%        
bounds of a representative set of trials (optional - can be entire trace)
number of sets (optional - can be 1 set)
any incomplete sets (optional)
flag for whether trial markers reset to 0 before next trial 
(usually the case when ao is stopped between trials
( = detect pairs of near-identical diff(marker) )
    or they go directly to the next trial/level 
       ( = half as many pks in (diff(marker), no pairs )

