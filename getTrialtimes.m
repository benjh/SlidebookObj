function getTrialtimes(obj)
% Bring up a plot of DAQ AI1 / wstim(:,2) and ask questions until all
% trials and repetition sets for the current tiff file are identified.

if isempty(obj.Daq)
    getFrametimes(obj);
end

if ~isempty(obj.trialparams)
    trialparams = obj.trialparams;
else
    trialparams = struct;
end

if ~isfield(trialparams,'chan') && (~obj.unattended)
    tfig = figure;
    tfig1 = subplot(2,1,1);
    tfig2 = subplot(2,1,2);
    plot(tfig1,obj.Daq(:,2))
    title(tfig1,'Channel 2');
    plot(tfig2,obj.Daq(:,3))
    title(tfig2,'Channel 3');
    % suptitle('Pick channel in the Command Window')
    trialparams.chan = input(['Which channel best marks trial times? Usually this is Channel 2.' ...
        '\n Enter [2] or [3]: ']);
    
    % Push to obj
    obj.trialparams.chan = trialparams.chan;
else trialparams.chan = 2;
end

if ~isfield(trialparams,'setlimits') && (~obj.unattended) && (~obj.polexp)
    % Draw a box around a single set (can be entire trace, can incl. multiple
    % trials )
    [trialparams.setlimits(1), trialparams.setlimits(2)] = drawSet(obj,trialparams);
    
    % Push to obj
    obj.trialparams.setlimits = trialparams.setlimits;    
    
    close(tfig)

else trialparams.setlimits = [1 length(obj.Daq)];
end

% First attempt at getting trials:
trialparams.plotflag = 1;
[trystarts, tryends] = getTrials(obj,trialparams);

if ~obj.unattended
    title('Inspect Example Set (reply in Command Window)')
    [trialstarts, trialends] = TroubleshootTrials(obj,trialparams,trystarts,tryends);
    
else
    title('Unattended mode is on. These have been pushed to obj')
end

% When finished, push to obj :
obj.TrialStartDAQsamples = trialstarts;
obj.TrialEndDAQsamples = trialends;

[obj.TrialStartFrames] = nextframe(obj,trialstarts);
obj.TrialEndFrames = prevframe(obj,trialends);



%     % Get the trial length in seconds and check this is in the right
%     % ballpark:
%     triallength_s = (trialends - trialstarts)/10000;
%     if mean(triallength_s) > (time_rest + 2*time_expt)*1.1  ||  mean(triallength_s) < (time_rest + 2*time_expt)*0.9
%         error(['Error in trial length: didx = ',num2str(didx),', tidx = ',num2str(tidx)])
%     end

% small peaks not detected?
% ntruials
% marker returns to zero?

% title(tfig,'How many complete sets identical to this?');
% Nsets = input(['WHow many complete sets identical to this? Usually this is only 1.' ...
%           '\n Enter: ']);

% f0 region?

function [trialstarts, trialends] = TroubleshootTrials(obj,trialparams,trystarts,tryends)
continueflag = 1;
while continueflag
    switch input('Do all trials look correct? [y/n]\nEnter response: ','s')
        case 'y'
            continueflag = 0;
            trialstarts  = trystarts;
            trialends = tryends;
        case 'n'
            switch input([...
                    '\nNumber of trials is incorrect             [1]' ...
                    '\nNumbers of Starts/Stops are not equal     [3]' ...
                    '\nStart/Stop on the 1st trial are mixed up  [5]' ...
                    '\nQuit                                      [0]' ...
                    '\nEnter response: '])
                case 1 % Total number of trials is incorrect
                    switch input([...
                            '\nTry to change peak detection thresholds   [1]' ...
                            '\nManually enter trial Start/Stop           [3]' ...
                            '\n This is a pol experiment                 [5]' ...
                            '\nQuit                                      [0]' ...
                            '\nEnter response: '])
                        case 1 % Try to change peak detection thresholds
                            figure(93)
                            plot(diff(obj.Daq(:,chan)),'r','LineWidth',2)
                            trialparams.minpeak = input(['' ...
                                '\nIdentify the amplitude of the smallest peak in red. ' ...
                                '\nEnter value: ']);
                            [trialstarts trialends] = getTrials(obj,trialparams);
                            
                        case 3
                            if input(['\nWould you like to overlay other channel data?'...
                                    '\nEnter response: [0/1] '])
                                figure(92)
                                hold on
                                chans = [2 3 4 5];
                                chans(chans == trialparams.chan) = [];
                                plot(obj.Daq(:,chans))
                            end
                            trialstarts(end+1) = input('Enter new trial Start: ');
                            trialends(end+1) = input('Enter new trial Stop: ');
                            trialstarts = sort(trialstarts,'descend');
                            trialends = sort(trialends,'descend');
                            checkTrials(obj, trialparams, trialstarts, trialends );
                        case 5
                            trialparams.pol = 1;
                            [trialstarts trialends] = getTrials(obj,trialparams);
                            
                    end
                    
                    
                case 3 % Different number of trial starts / stops
                case 5 % First trial mixed up start / stop
                    trialparams.firstTrialError = 1;
                    [trialstarts trialends] = getTrials(obj,trialparams);
                    
                    % case 7 % Manually add samples before / after trials
                    % Every trial
                    % First or last trials only
                    
                case 0 % Quit
                    return
            end
    end
end


function [setmin, setmax, fig] = drawSet(obj,trialparams)

figure(91)
plot(obj.Daq(:,trialparams.chan))
title([{'Drag the box around a single set of trials'},{'(this may be the entire trace)'}])
h = imrect;
% pos = wait(h)
pause
pos = getPosition(h);
setmin = round(pos(1));
setmax = round(pos(3) + setmin);
if setmin<0, setmin = 1; end
if setmax>length(obj.Daq), setmax = length(obj.Daq); end
delete(h); clear h
ylims = ylim;
hold on,
line([setmin setmin], [ylims(1) ylims(2)],'color','r','linewidth',2,'linestyle',':');
line([setmax setmax], [ylims(1) ylims(2)],'color','r','linewidth',2,'linestyle',':');

function checkTrials(obj, trialparams, trialstarts, trialends )
% Plot to check trial start/stops   
wstim = obj.Daq;
figure(92)
plot( wstim(:,trialparams.chan) )
hold on
plot(trialstarts,wstim(trialstarts,trialparams.chan),'k*')
plot(trialends,wstim(trialends,trialparams.chan),'g*')
hold off
xlim(trialparams.setlimits);
legend(['wstim(:,' num2str(trialparams.chan) ')'],['Trial starts (' num2str(length(trialstarts)) ')'],['Trial stops (' num2str(length(trialends)) ')'])

function [trialstarts, trialends] = getTrials(obj,trialparams)
% obj,setlimits,minpeak,distinct

wstim = obj.Daq;

if isempty(obj.ExpParams)
    getParameters(obj);
end


if nargin < 2
    if ~isempty(obj.trialparams)
        % Get stored parameters
        t = obj.trialparams;
    else
        % Defaults:
        t.chan = 2;
        t.minpeak = 0.1;
        t.joined = 0;
        t.plotflag = 0;
        t.firstTrialError = 0;
    end
    
elseif nargin == 2
    % Use parameters specified in input
    if isfield(trialparams,'chan')
        t.chan = trialparams.chan;
    else t.chan = 2;
    end
    if isfield(trialparams,'setlimits')
        t.setlimits = trialparams.setlimits;
    else
        t.setlimits = [1 length(obj.Daq)];
    end
    if isfield(trialparams,'minpeak')
        t.minpeak = trialparams.minpeak;
    else t.minpeak = 0.1;
    end
    if isfield(trialparams,'plotflag')
        t.plotflag = trialparams.plotflag;
    else t.plotflag = 0;
    end
    if isfield(trialparams,'joined')
        t.joined = trialparams.joined;
    end
    if isfield(trialparams,'firstTrialError')
        t.firstTrialError = trialparams.firstTrialError;
    end
    %     if isfield(trialparams,'polexp')
    %         t.polexp = trialparams.polexp;
    %     end
end

% Find trial start/stop times (in samples)

% This is a simple, fast method:
% Get the differential of the signal (to locate sudden steps)
% Use inequalities to locate the peaks within an appropriate range:
%    lower bound may have to be queried
%    upper bound is set by 10V range of analog input


trialstarts = find(diff(wstim(:,t.chan)) >= t.minpeak  &  diff(round(wstim(:,t.chan))) < 15 );
trialends =  find(-diff(wstim(:,t.chan)) >= t.minpeak  & -diff(round(wstim(:,t.chan))) < 15 );

% Discard any detected peaks which are too close to a previous trial to
% be real:
%   100 samples = 1 ms
trialstarts(find(diff(trialstarts)<100)+1) = [];
trialends(find(diff(trialends)<100)+1) = [];

if  ~obj.polexp
    % Discard (for now) trials outside of the current Set
    xrange = t.setlimits(1):t.setlimits(2);
    trialstarts( trialstarts<t.setlimits(1) | trialstarts>t.setlimits(2) ) = [];
    trialends( trialends<t.setlimits(1) | trialends>t.setlimits(2) ) = [];
end

if isfield(t,'firstTrialError') && t.firstTrialError
    % If wstim(:,chan) was not reset to 0 at the start of the experiment,
    % the first trialstart may be missed.
    if round(mean(wstim(1:100,t.chan)*10)) ~= 0
        % A trialend may also be erroneously detected at the start, so swap
        % these around:
        if (trialends(1) < trialstarts(1))
            trialstarts(2:length(trialstarts)+1) = trialstarts;
            trialstarts(1) = trialends(1);
            trialends(1) = [];
        end
        
    end
end

% Custom code for finding trials in polarization experiments
% 0deg trials are recorded as 0V on the AI channel, so they can't be
% automatically detected. Here we fill in where they should be:
if  obj.polexp
    
    % We will run through all complete Sets of trials. For Pol Exps run in
    % continuous mode, we can detect these Sets in Daq(:,3), because the LEDpow
    % signal is copied here and remains ON for each Set.
    % So we first look for rising and falling edges equal in size to LEDpow:
    assert(obj.ExpParams.continuous == 1,'ExpsParams.continuous = 0. Different Set detection required.')
    
    setstarts = find(diff(wstim(:,3)) >= obj.ExpParams.LEDpow*0.95);
    setends =  find(-diff(wstim(:,3)) >= obj.ExpParams.LEDpow*0.95);
    
    % Discard any detected peaks which are too close to a previous trial to
    % be real:
    %   100 samples = 1 ms
    setstarts(find(diff(setstarts)<100)+1) = [];
    setends(find(diff(setends)<100)+1) = [];
    
    % Also discard any setends which occurred before the first setstart
    % (these can occur from Intensity Ramping)
    setends(setends<setstarts(1)) = [];
    
    % Take a copy of trialstarts and trialstops detected
    Tstartcopy = trialstarts;
    Tendscopy = trialends;
    
    % And allocate space for the trials which will be retained from each set:
    Tsetstarts = [];
    Tsetends = [];
    
    % Now we loop through the complete sets detected
    for sidx = 1:length(setstarts)
        
        % Restore all trials detected
        trialstarts = Tstartcopy;
        trialends = Tendscopy;
        
        % Change the limits for this Set
        t.setlimits(1) = setstarts(sidx);
        t.setlimits(2) = setends(sidx);
        
        % Discard (for now) trials outside of the current Set
        trialstarts( trialstarts<t.setlimits(1) | trialstarts>t.setlimits(2) ) = [];
        trialends( trialends<t.setlimits(1) | trialends>t.setlimits(2) ) = [];
        xrange = t.setlimits(1):t.setlimits(2);
        
        if isfield(t,'firstTrialError') && t.firstTrialError
            % If wstim(:,chan) was not reset to 0 at the start of the experiment,
            % the first trialstart may be missed.
            if round(mean(wstim(1:100,t.chan)*10)) ~= 0
                % A trialend may also be erroneously detected at the start, so swap
                % these around:
                if (trialends(1) < trialstarts(1))
                    trialstarts(2:length(trialstarts)+1) = trialstarts;
                    trialstarts(1) = trialends(1);
                    trialends(1) = [];
                end
                
            end
        end
        
        % Custom code for finding trials in polarization experiments
        % 0deg trials are recorded as 0V on the AI channel, so they can't be
        % automatically detected. Here we fill in where they should be:
        %     if  obj.polexp == 1
        
        % Mean trial interval for later:
        iti = round(mean(trialends - trialstarts));
        
        % (Nreps - 1) trials missed in the center:
        
        % Assuming all trials have been correctly identified, we can just look
        % for the largest gaps between trialstarts:
        for nidx = 1:obj.ExpParams.nreps -1
            
            [~,s] = max(diff(trialstarts));
            
            % Then the correct range of the gap is:
            % trialends(s) : trialstarts(s+1)
            gaplength = trialstarts(s+1) - trialends(s);
            
            if gaplength > iti
                
                interval = floor(0.5*(gaplength - iti));
                trialstarts(end+1) = trialends(s) + interval;
                trialends(end+1) = trialends(s) + interval + iti;
                
                % Rearrange trials in order every time we add new entries
                trialstarts = sort(trialstarts,'ascend');
                trialends = sort(trialends,'ascend');
                
            else
                
                checkTrials(obj, t, trialstarts, trialends );
                
                if (nidx == obj.ExpParams.nreps -1) && ...
                        (length(wstim) < trialends(end) + iti)
                    % This is an incomplete Set of trials
                    disp('Last Set of trials is incomplete')
                else
                    title('One or more 0 degree trials have not been detected.')
                end
                
            end
            
        end
        
        % A 0 degree trial is missed  at the start:
        
        % We can detect where the LED first comes on for this Set
        % in channel 3 timeseries, near the beginning:
        LEDswitched = (diff(wstim(:,3)));
        
        % Mean trial interval tells us roughly how far in advance to check
        temprange(1) = trialstarts(1) - round(1.1*iti);
        temprange(2) = trialstarts(1);
        
        % Look for the highest peak within this starting range
        % Corresponds to LED ON (rising edge) so diff should be positive
        LEDswitched(1: temprange(1) ) = 0;
        LEDswitched(temprange(2):end) = 0;
        [~,m] = max(LEDswitched);
        
        trialstarts(end+1) = m;
        trialends(end+1) = m+iti;
        
        
        % Rearrange trials in order
        trialstarts = sort(trialstarts,'ascend');
        trialends = sort(trialends,'ascend');
        
        
        Tsetstarts(length(Tsetstarts)+1 : length(Tsetstarts)+length(trialstarts)) = trialstarts;
        Tsetends(length(Tsetends)+1 : length(Tsetends)+length(trialends)) = trialends;
        
    end
    
    % After all Sets, store the final output variables for checking
    clear t.setlims trialstarts trialends
    t.setlimits(1) = 1;
    t.setlimits(2) = length(wstim);
    trialstarts = Tsetstarts;
    trialends = Tsetends;
end

if t.plotflag
    checkTrials(obj, t, trialstarts, trialends );
end

