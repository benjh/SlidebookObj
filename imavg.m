function imavg(obj)

if isempty(obj.AverageFrame)
    
    if isempty(obj.Frames)        
        getFrames(obj);    
    end

    obj.AverageFrame = (mean(obj.Frames,3));
end

figure,
imshow(obj.AverageFrame,[])

