function fig = play(obj,vid_extent)

if nargin < 2
    vid_extent = [1 0];
end

if isempty(obj.Frames)
    mov = getFrames(obj,vid_extent);
end

nia_sbo_playFlatMovie(mov)