function plotPolAvgs(obj,IDX1, IDX2)

if isempty(obj.AngleStep)
getAuxDaqData(obj)
end
if isempty(obj.imF0)
    adhocF0(obj)
end

ang = obj.AngleStep;
angles = [0:ang:360-ang];

if nargin <2

    IDX1 = 1;
    IDX2 = ceil(0.25*length(angles));    
end

for idx = 1:length(angles)
    
% First extract all trials which have a certain angle
    aa = obj.TrialStartFrames;
    aaa = aa([obj.TrialAngles]==angles(idx));
    zz = obj.TrialEndFrames;
    zzz = zz([obj.TrialAngles]==angles(idx));
    
    clear angframe
    % Then loop through each trial 
    for nidx = 1:length(aaa)
        
        % Get the frames within the trial and store their mean
        trialframes = [obj.Frames(:,:,aaa(nidx):zzz(nidx))];
        trialavgs(:,:,nidx) = mean(trialframes,3);
        
    end
    
    % Now get the mean img of all trials at this angle
    angleavg(:,:,idx) = mean(trialavgs,3);
    
end

% This is used to help remove the background
imgvar = std(angleavg,[],3);

% Colormap which has full rainbow - good for POL stuff
cmap = hsv(128);

figure
for idx = 1:length(angles);   
    
    % Subtract off the 'background' F0
    imgbksubtr = abs( angleavg(:,:,idx) - obj.imF0 );

    % Enhance regions which are most variable
    a = (imgbksubtr.*(imgvar./max(max(imgvar))));
    
    % Scale
    a = 2*a/max(max(a));
    
    subplot(length(angles)/6,6,idx)
    
cidx = mod(idx-1,0.5*length(angles))+1;
    

% RGB - better for indiv images
cr = cmap(round(cidx*length(cmap)/(0.5*length(angles))),:);   
      
b = cat(3,a.*cr(1),a.*cr(2),a.*cr(3));
imshow(b,[])
title(num2str(angles(idx)))

% MONO -better for imshowpair - this is the one we save
cm = [1 1 1];
    
  b = cat(3,a.*cm(1),a.*cm(2),a.*cm(3));     
    
    if idx > 0.5*length(angles)
        tempcombi(:,:,:,cidx,2) = b;
    else
        tempcombi(:,:,:,cidx,1) = b;
    end
end


clear combi
figure
for cidx = 1:0.5*length(angles)

    d = mean(tempcombi(:,:,:,cidx,:),5);
    subplot(1,6,cidx)
    imshow(1-d,[],'InitialMagnification','fit')
    title([ num2str(angles(cidx)) '^{\circ}/' num2str(angles(cidx+0.5*length(angles))) '^{\circ}'])
    
    combi(:,:,:,cidx) = d;
end

clear merged 
merged = combi(:,:,:,1);
for cidx = 2:0.5*length(angles)
merged = merged - squeeze(combi(:,:,:,cidx));
end
% figure
% imshow(merged,[])

figure
imshowpair(1-3*combi(:,:,:,IDX1),1-3*combi(:,:,:,IDX2),'colorchannels','red-cyan')
title([ num2str(angles(IDX1)) '^{\circ} vs. ' num2str(angles(IDX2))  '^{\circ}'])