function frame = prevframe(obj,sampidx) 
% Shortcut to find the prev frame from a particular timepoint in DAQ data
for idx = 1:length(sampidx)
tempframe = find(obj.frametimes<=sampidx(idx) & obj.frametimes>sampidx(idx)-obj.ifi);
frame(idx) = tempframe(end);
end