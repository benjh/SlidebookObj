function runTifReg(obj, poolSiz, walkThresh)
NETWORK_FILE = 0;
% If file is not on C:\ or D:\ it's likely on a portable or network drive
if ~strcmpi(obj.Folder(1), 'c') && ~strcmpi(obj.Folder(1), 'd')
    NETWORK_FILE = 1;
    % .. so copy tiff file to a new local folder
    local_root = 'C:\tempMatlab\';
    local_dir = [local_root obj.File '\'];
    network_filepath = [obj.Folder obj.Tiff];
    
    % Make the folder if it doesn't exist
    if exist(local_root, 'dir')
        
        % remove and remake
        % Matlab can't remove the folder if it's the current
        % working directory, so move one folder up
        if strcmp(local_dir,cd)
            cd('\')
        end
        
        % Remove non-empty folder
        rmdir(local_root,'s')
    end
    
    
   % re-make local root folder with the same name
    mkdir(local_root)
    
    %{
%         % or notify that it already exists
        
%         disp(['Attempting to copy files to local drive but file/folder already exists!'])
%
%         delflag = [];
%
%         % and whether it has files in or not
%         if isempty(what(local_dir))
%             disp('Folder is empty..')
%         else
%             disp('Folder is NOT empty!')
%         end
%
%         % query for delete folder or use existing files
%         while ~strcmp(delflag,'y') && ~strcmp(delflag,'n')
%
%             [delflag]=input('Delete existing copy on C:\\ [y] or use existing local files [n]? ','s');
%
%             if strcmp(delflag, 'y')
%
%                 % Matlab can't remove the folder if it's the current
%                 % working directory, so move one folder up
%                 if strcmp(local_dir,cd)
%                     cd('\')
%                 end
%                 % Remove non-empty folder and re-make with the same name
%                 rmdir(local_dir,'s')
%                 mkdir(local_dir)
%                 disp(['Copying files to ' local_dir,''])
%                 copyfile(network_filepath, local_dir)
%
%             elseif strcmp(delflag, 'n')
%                 disp('Using existing local files.')
%             else
%                 disp('Please enter [y] or [n] only.')
%             end
%         end
    %}
    
    mkdir(local_dir)
    disp(['Copying files to ' local_root ''])
    copyfile(network_filepath, local_dir)
    
else
    local_dir = [obj.Folder];
end

    
if nargin < 3,
    walkThresh = 5; % hard threshold to identify big jerks that need extra attention
end

if nargin <2
    poolSiz = 400;
end

ca_RegSeries_v3_StandAlone_InputArgs(obj.Tiff, local_dir, poolSiz, walkThresh);

if NETWORK_FILE
    % Copy reg_tif back to original folder on network drive
    copyfile([local_dir obj.TifReg], obj.Folder)
    rmdir(local_root, 's')
    disp('Files moved back to network drive.')
end
