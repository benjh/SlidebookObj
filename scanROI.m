function out = scanROI(obj, mask, scan_extent)
%SCANROI Find mean value within roi across frames
%   out = scanROI(obj, scan_extents, mask) finds the mean value of the
%   pixels within the passed ROI for each frame within the passed SlidebookObj. 
%   This function accepts the following arguments:
%
%       mask - A N-dimensional logical array the same size as the image
%           data with true values for pixels that should be included in the
%           ROI. For efficiency, all image data must have the dimensions.
%           If image data is encountered with a different dimension, then
%           that slice is given a value of NaN.
%
%       scan_extents - A [2x1] array that specifies how to scan through the
%           position data. The first element specifies the first frame to scan, 
%           and the second element specifies the last frame to scan to. 
%           If one or more entries is missing, the entire tiff will be
%           scanned.
%
%
%   This function has the following outputs:
%
%       out - A [3xN] vector where N is equal to the number of processed
%           slices. The first row contains the slice index from which the
%           ROI was calculated, and the second row contains the time
%           associated with the slice, and the third row contains the
%           calculated ROI value.
%
%        mean pixel intensity
%

% Check input arguments

%    if empty or only [1x1] then discard and use full extent
if nargin==3 && ~isempty(scan_extent) 
%   scan_extent should be [2x1]
%   scan_extent(2) > scant_extent(1)
elseif nargin<3 || isempty(scan_extent) || length(scan_extent) < 2  
    scan_extent = [1 0];
end


% Check ROI is same size as tiff frames
clearflag = 0;
% Check if Frames have already been scanned into object
if isempty(obj.Frames) && isequal(scan_extent,[1 0])
    
    % If not, and all Frames are requested, we will store them in the obj 
    getFrames(obj);
    % And adjust scan_extent to scan to the last frame
    scan_extent = [1 size(obj.Frames,3)];
    
elseif isempty(obj.Frames)
    % Or read only the frames requested
   obj.Frames = getFrames(obj,scan_extent);
   % We store them temporarily in obj.Frames
   % Set a marker for deleting them at the end of the function
   clearflag = 1;
   % And shift scan_extent to reflect the frames being at the start of
   % obj.Frames
   scan_extent = [1 1+scan_extent(2)-scan_extent(1)];
   
elseif ~isempty(obj.Frames) && isequal(scan_extent,[1 0])
    scan_extent = [1 size(obj.Frames,3)];
end



%   If no ROI specified then use entire image frame
if isempty(mask)
    disp('No ROI specified: finding mean of entire image')
    mask = true(size(obj.Frames,1),size(obj.Frames,2));
end

% If an ROI was specified it must be the same size as the tiff image frames
assert(isequal(size(mask),size(obj.Frames(:,:,1))) ,'ROI size does not match dimensions of tiff frames')
if ~islogical(mask)
    mask = logical(mask);
end

n = scan_extent(1) - 1;  % Frame counter
m = 0; 
while (m == 0) || ( n ~= scan_extent(2) )
    n = n + 1;
    m = m + 1;
    % Grab the current frame
    currentframe = obj.Frames(:,:,n);    
    % Extract the mean pixel intensity within the ROI
    out(m) = mean(currentframe(mask));
end
    
if clearflag
    obj.Frames = [];
end

